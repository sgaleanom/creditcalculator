#language: en

Feature: validate the operation of credit calculator

#  Scenario Outline: Sebastian wants to calculate how much money they can lend him
#    Given Sebastian enters the calculator web portal
#    And he opens the amounts calculator
#    When he enters the value of his income <monthly income> and the payment <payment term in years>
#    Then he can see the amount of money that the bank lends him and the conditions for his credit
#
#    Examples:
#      | monthly income | payment term in years |
#      | 6000000        | 15 Años               |
#      | 7500000        | 20 Años               |

  Scenario Outline: Sebastian wants to calculate the value of his monthly payment
    Given Sebastian enters the calculator web portal
    And he opens the fee calculator
    When he enters the credit value <credit value>,the payment <payment term in years> and the interest rate <interest rate>
    Then he can see the amount of fees and the credit conditions

    Examples:
      | credit value | payment term in years | interest rate |
      | 200000000    | 15 Años               | 10.25         |
      | 350000000    | 20 Años               | 10.25         |

