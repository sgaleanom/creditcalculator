#language: en

Feature: validate the operation of credit calculator

  Scenario Outline: Sebastian wants to calculate how much money they can lend him
    Given Sebastian entered the calculator web portal
    And he opened the amounts calculator
    When he enters the value of his income <monthly income> and the payment <payment term in years> and the interest rate <interest rate>
    Then He will be able to see the amount of money that the bank will lend him and the conditions of his credit

    Examples:
      | monthly income | payment term in years | interest rate |
      | 6000000        | 15 Años               | 0.8165        |
      | 6000000        | 20 Años               | 0.8165        |

  Scenario Outline: Sebastian wants to calculate the value of his monthly payment
    Given Given Sebastian entered the calculator web portal
    And he opened the fee calculator
    When he enters the credit value <credit value>,the payment <payment term in years> and the interest rate <interest rate>
    Then He will be able to see the amount of the fees and the credit conditions

    Examples:
      | credit value | payment term in years | interest rate |
      | 20000000     | 15 Años               | 0.8165        |
      | 35000000     | 20 Años               | 0.8165        |

