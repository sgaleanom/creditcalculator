package com.credit.calculator.question;

import static com.credit.calculator.components.CreditSimulation.LBL_AMOUNT_FEE_AMOUNT_CALCULATOR;
import static com.credit.calculator.components.CreditSimulation.LBL_INITIAL_FEE_CALCULATOR;
import static com.credit.calculator.components.CreditSimulation.LBL_TOTAL_CREDIT_VALUE_FEE_CALCULATOR;
import static com.credit.calculator.model.StepAssertion.setStepExceptionSerenitySession;
import static com.credit.calculator.utils.Operations.extractNumericalCode;
import static com.credit.calculator.utils.Operations.getFeesData;

import com.credit.calculator.model.FeesData;
import com.credit.calculator.model.StepAssertion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.thucydides.core.annotations.Step;

@Subject("Confirm fees data")
public class ConfirmFees implements Question<Boolean> {

  @Override
  @Step("{0} Valifate fee data, monthly fee, property value and minimum initial fee")
  public Boolean answeredBy(Actor actor) {
    FeesData feesData = getFeesData();
    FeesData feesDataPage = new FeesData();
    feesDataPage.setMinimumFee(
        Double.parseDouble(
            extractNumericalCode(LBL_INITIAL_FEE_CALCULATOR.resolveFor(actor).getText())));
    feesDataPage.setMonthlyFee(
        Double.parseDouble(
            extractNumericalCode(LBL_AMOUNT_FEE_AMOUNT_CALCULATOR.resolveFor(actor).getText())));
    feesDataPage.setTotalamount(
        Double.parseDouble(
            extractNumericalCode(
                LBL_TOTAL_CREDIT_VALUE_FEE_CALCULATOR.resolveFor(actor).getText())));
    StepAssertion stepAssertion = new StepAssertion();
    if (!(feesData.getMonthlyFee() - feesDataPage.getMonthlyFee() < 1000
        && feesData.getMonthlyFee() - feesDataPage.getMonthlyFee() > -1000)) {
      stepAssertion.setMessageError(
          "Error in the value 'Value of the monthly fee, fee calculator'");
      stepAssertion.setStepFail(false);
    }
    if (!(feesData.getTotalamount() - feesDataPage.getTotalamount() < 1000
        && feesData.getTotalamount() - feesDataPage.getTotalamount() > -1000)) {
      stepAssertion.setMessageError(
          "Error in the value 'Value you can buy a property from, Fee Calculator'");
      stepAssertion.setStepFail(false);
    }
    if (!(feesData.getMinimumFee() - feesDataPage.getMinimumFee() < 1000
        && feesData.getMinimumFee() - feesDataPage.getMinimumFee() > -1000)) {
      stepAssertion.setMessageError("Error in the value 'Value You must have monthly income of'");
      stepAssertion.setStepFail(false);
    }
    setStepExceptionSerenitySession(stepAssertion);
    return stepAssertion.isStepFail();
  }

  public static ConfirmFees data() {
    return new ConfirmFees();
  }
}
