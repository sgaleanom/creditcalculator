package com.credit.calculator.question;

import static com.credit.calculator.components.CreditSimulation.LBL_AMOUNT_FEE_CALCULATOR;
import static com.credit.calculator.components.CreditSimulation.LBL_AMOUNT_LOAN_CREDIT_CALCULATOR;
import static com.credit.calculator.components.CreditSimulation.LBL_INITIAL_FEE_AMOUNT_CALCULATOR;
import static com.credit.calculator.components.CreditSimulation.LBL_TOTAL_CREDIT_VALUE_AMOUNT_CALCULATOR;
import static com.credit.calculator.model.StepAssertion.setStepExceptionSerenitySession;
import static com.credit.calculator.utils.Operations.extractNumericalCode;
import static com.credit.calculator.utils.Operations.getAmountData;

import com.credit.calculator.model.AmountData;
import com.credit.calculator.model.StepAssertion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.thucydides.core.annotations.Step;

@Subject("Confirm credit data")
public class ConfirmCredit implements Question<Boolean> {

  @Override
  @Step("{0} Validate credit data, property value, loan bank, minimum initial fee and mounthly fee")
  public Boolean answeredBy(Actor actor) {
    AmountData amountData = getAmountData();
    AmountData amountDataPage = new AmountData();
    amountDataPage.setTotalamount(
        Double.parseDouble(
            extractNumericalCode(
                LBL_TOTAL_CREDIT_VALUE_AMOUNT_CALCULATOR.resolveFor(actor).getText())));
    amountDataPage.setBankLoan(
        Double.parseDouble(
            extractNumericalCode(LBL_AMOUNT_LOAN_CREDIT_CALCULATOR.resolveFor(actor).getText())));
    amountDataPage.setMinimumFee(
        Double.parseDouble(
            extractNumericalCode(LBL_INITIAL_FEE_AMOUNT_CALCULATOR.resolveFor(actor).getText())));
    amountDataPage.setMonthlyFee(
        Double.parseDouble(
            extractNumericalCode(LBL_AMOUNT_FEE_CALCULATOR.resolveFor(actor).getText())));
    StepAssertion stepAssertion = new StepAssertion();
    if (amountData.getMonthlyFee() != amountDataPage.getMonthlyFee()) {
      stepAssertion.setMessageError("Error in the value 'Value of the monthly fee'");
      stepAssertion.setStepFail(false);
    }
    if (!(amountData.getTotalamount() - amountDataPage.getTotalamount() < 5000
        && amountData.getTotalamount() - amountDataPage.getTotalamount() > -5000)) {
      stepAssertion.setMessageError("Error in the value 'Value you can buy a property from'");
      stepAssertion.setStepFail(false);
    }
    if (!(amountData.getMinimumFee() - amountDataPage.getMinimumFee() < 5000
        && amountData.getMinimumFee() - amountDataPage.getMinimumFee() > -5000)) {
      stepAssertion.setMessageError(
          "Error in the value 'Value You must have a minimum down payment of'");
      stepAssertion.setStepFail(false);
    }
    if (!(amountData.getBankLoan() - amountDataPage.getBankLoan() < 5000
        && amountData.getBankLoan() - amountDataPage.getBankLoan() > -5000)) {
      stepAssertion.setMessageError("Error in the value 'Value A bank can lend you up to'");
      stepAssertion.setStepFail(false);
    }
    setStepExceptionSerenitySession(stepAssertion);
    return stepAssertion.isStepFail();
  }

  public static ConfirmCredit data() {
    return new ConfirmCredit();
  }
}
