package com.credit.calculator.exceptions;

import static com.credit.calculator.model.StepAssertion.getStepExceptionSerenitySession;

public class TheCreditDataIsIncorrect extends AssertionError {

  public TheCreditDataIsIncorrect(String message, Throwable cause) {
    super(getStepExceptionSerenitySession().getMessageError(), cause);
  }
}
