package com.credit.calculator.model;

import net.serenitybdd.core.Serenity;

public class StepAssertion {

  private boolean stepFail = true;
  private String messageError = "";

  public StepAssertion() {}

  public boolean isStepFail() {
    return stepFail;
  }

  public void setStepFail(boolean stepFail) {
    this.stepFail = stepFail;
  }

  public String getMessageError() {
    return messageError;
  }

  public void setMessageError(String messageError) {
    this.messageError += messageError + "\n";
  }

  public static void setStepExceptionSerenitySession(StepAssertion stepAssertion) {
    Serenity.setSessionVariable("step assertion").to(stepAssertion);
  }

  public static StepAssertion getStepExceptionSerenitySession() {
    return Serenity.sessionVariableCalled("step assertion");
  }
}
