package com.credit.calculator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FeesData {

  private double totalamount;
  private double minimumFee;
  private double monthlyFee;
}
