package com.credit.calculator.utils;

public enum SessionVariables {
  MOUNTHLY_INCOME("monthly income"),
  CREDIT_VALUE("credit value"),
  INTEREST_RATE("interest rate"),
  PAYMENT_TERM("payment term");
  ;

  private final String sessionVariable;

  SessionVariables(String sessionVariable) {
    this.sessionVariable = sessionVariable;
  }

  public String getSessionVariable() {
    return sessionVariable;
  }
}
