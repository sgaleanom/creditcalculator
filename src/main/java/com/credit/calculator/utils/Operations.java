package com.credit.calculator.utils;

import static com.credit.calculator.utils.SessionVariables.CREDIT_VALUE;
import static com.credit.calculator.utils.SessionVariables.INTEREST_RATE;
import static com.credit.calculator.utils.SessionVariables.MOUNTHLY_INCOME;
import static com.credit.calculator.utils.SessionVariables.PAYMENT_TERM;

import com.credit.calculator.model.AmountData;
import com.credit.calculator.model.FeesData;
import java.text.Normalizer;
import net.serenitybdd.core.Serenity;

public class Operations {

  public static double calculateMonthlyFee(double loanValue, int time, double rate) {
    double r = rate / 100;
    double numerator = loanValue * r;
    double dm = Math.pow((1 + r), (-time * 12));
    double denominator = 1 - dm;
    return numerator / denominator;
  }

  public static double calculateCreditAmount(double feeAmount, int time, double rate) {
    double r = rate / 100;
    double nm = Math.pow((1 + r), (-time * 12));
    double num = 1 - nm;
    double numerator = feeAmount * num;
    return numerator / r;
  }

  public static String extractNumericalCode(String text) {
    return Normalizer.normalize(text, Normalizer.Form.NFD)
        .replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
        .replaceAll("[a-zA-Z\\,\\$\\.]", "")
        .trim();
  }

  public static AmountData getAmountData() {
    long monthlyIncome = Serenity.sessionVariableCalled(MOUNTHLY_INCOME.getSessionVariable());
    int years =
        Integer.parseInt(
            extractNumericalCode(
                Serenity.sessionVariableCalled(PAYMENT_TERM.getSessionVariable())));
    double interestRate = Serenity.sessionVariableCalled(INTEREST_RATE.getSessionVariable());
    double monthlyFee = monthlyIncome * 0.3;
    double totalAmount = calculateCreditAmount(monthlyFee, years, interestRate) / 0.7;
    double minimumFee = totalAmount * 0.3;
    double bankLoan = totalAmount * 0.7;
    return new AmountData(monthlyFee, totalAmount, minimumFee, bankLoan);
  }

  public static FeesData getFeesData() {
    long creditValue = Serenity.sessionVariableCalled(CREDIT_VALUE.getSessionVariable());
    double interestRate = Serenity.sessionVariableCalled(INTEREST_RATE.getSessionVariable());
    int years =
        Integer.parseInt(
            extractNumericalCode(
                Serenity.sessionVariableCalled(PAYMENT_TERM.getSessionVariable())));
    double monthlyFee = calculateMonthlyFee(creditValue, years, interestRate);
    double totalAmount = creditValue / 0.7;
    double minimumFee = totalAmount * 0.3;
    return new FeesData(totalAmount, minimumFee, monthlyFee);
  }
}
