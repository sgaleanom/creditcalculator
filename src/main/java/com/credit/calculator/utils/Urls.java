package com.credit.calculator.utils;

public enum Urls {
  CREDIT_CALCULATOR("https://www.metrocuadrado.com/calculadora-credito-hipotecario-vivienda/");

  private final String url;

  Urls(String url) {
    this.url = url;
  }

  public String Url() {
    return url;
  }
}
