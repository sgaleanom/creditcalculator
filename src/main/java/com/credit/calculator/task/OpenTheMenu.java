package com.credit.calculator.task;

import static com.credit.calculator.components.MainMenu.BTN_AMOUNT_CALCULATOR;
import static com.credit.calculator.components.MainMenu.BTN_FEE_CALCULATOR;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

public class OpenTheMenu implements Task {

  private String option;
  private static final String AMOUNTS_CALCULATOR = "amounts calculator";
  private static final String FEE_CALCULATOR = "fee calculator";

  public OpenTheMenu(String option) {
    this.option = option;
  }

  @Override
  @Step("{0} select calculator function ")
  public <T extends Actor> void performAs(T actor) {
    if (option.equals(AMOUNTS_CALCULATOR)) {
      actor.attemptsTo(Click.on(BTN_AMOUNT_CALCULATOR));
    } else if (option.equals(FEE_CALCULATOR)) {
      actor.attemptsTo(Click.on(BTN_FEE_CALCULATOR));
    }
  }

  public static OpenTheMenu option(String option) {
    return Tasks.instrumented(OpenTheMenu.class, option);
  }
}
