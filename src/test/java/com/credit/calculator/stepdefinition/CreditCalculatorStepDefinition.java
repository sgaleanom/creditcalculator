package com.credit.calculator.stepdefinition;

import static com.credit.calculator.utils.SessionVariables.CREDIT_VALUE;
import static com.credit.calculator.utils.SessionVariables.INTEREST_RATE;
import static com.credit.calculator.utils.SessionVariables.MOUNTHLY_INCOME;
import static com.credit.calculator.utils.SessionVariables.PAYMENT_TERM;
import static com.credit.calculator.utils.Urls.CREDIT_CALCULATOR;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.is;

import com.credit.calculator.exceptions.TheCreditDataIsIncorrect;
import com.credit.calculator.question.ConfirmCredit;
import com.credit.calculator.question.ConfirmFees;
import com.credit.calculator.task.CalculateCredit;
import com.credit.calculator.task.CalculateFees;
import com.credit.calculator.task.OpenTheMenu;
import com.credit.calculator.task.OpenWeb;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class CreditCalculatorStepDefinition {

  @Before
  public void prepareStage() {
    setTheStage(new OnlineCast());
  }

  @Given("^(.*) entered the calculator web portal$")
  public void EnterPage(String actorsName) {
    theActorCalled(actorsName).wasAbleTo(OpenWeb.page(CREDIT_CALCULATOR.Url()));
  }

  @Given("^he opened the (.*)$")
  public void openCalculator(String option) {
    theActorInTheSpotlight().attemptsTo(OpenTheMenu.option(option));
  }

  @When("^he enters the value of his income (.*) and the payment (.*) and the interest rate (.*)$")
  public void enterValueIncomeAndThePayment(
      long monthlyIncome, String paymentTerm, double interestRate) {
    Serenity.setSessionVariable(MOUNTHLY_INCOME.getSessionVariable()).to(monthlyIncome);
    Serenity.setSessionVariable(PAYMENT_TERM.getSessionVariable()).to(paymentTerm);
    Serenity.setSessionVariable(INTEREST_RATE.getSessionVariable()).to(interestRate);
    theActorInTheSpotlight().attemptsTo(CalculateCredit.amounts(monthlyIncome, paymentTerm));
  }

  @Then(
      "^He will be able to see the amount of money that the bank will lend him and the conditions of his credit$")
  public void verifyAmountMoneyAndConditions() {
    theActorInTheSpotlight()
        .should(
            seeThat(ConfirmCredit.data(), is(true))
                .because(
                    "%s Confirmed credit data, property value, loan bank, minimum initial fee and mounthly fee %s")
                .orComplainWith(TheCreditDataIsIncorrect.class));
  }

  @When("^he enters the credit value (.*),the payment (.*) and the interest rate (.*)$")
  public void enterCreditValuePaymentAndInterestRate(
      long creditValue, String paymentTerm, double interestRate) {
    Serenity.setSessionVariable(CREDIT_VALUE.getSessionVariable()).to(creditValue);
    Serenity.setSessionVariable(PAYMENT_TERM.getSessionVariable()).to(paymentTerm);
    Serenity.setSessionVariable(INTEREST_RATE.getSessionVariable()).to(interestRate);
    theActorInTheSpotlight().attemptsTo(CalculateFees.ofCredit(creditValue, paymentTerm));
  }

  @Then("^He will be able to see the amount of the fees and the credit conditions$")
  public void verifyAmountFeesAndConditions() {
    theActorInTheSpotlight()
        .should(
            seeThat(ConfirmFees.data(), is(true))
                .because(
                    "%s Confirmed credit data, monthly fee, property value and minimum initial fee %s")
                .orComplainWith(TheCreditDataIsIncorrect.class));
  }
}
